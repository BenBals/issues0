defmodule CliTest do
  use ExUnit.Case

  import Issues.CLI


  test "gives back :help if --help or -h is given" do
    assert parse_args(["-h", "anything"]) == :help
    assert parse_args(["--help", "anything"]) == :help
  end

  test "three values retured if three given" do
    assert parse_args(["BenBals", "stammformen", "99"]) === {"BenBals", "stammformen", 99}
  end

  test "defaults to 4 if no count is given" do
    assert parse_args(["BenBals", "stammformen"]) == {"BenBals", "stammformen", 4}
  end

  test "sort acsending orders the right way" do
    result = sort_into_list_of_acsending_order(fake_created_at_list(["c", "a", "b"]))
    issues = for issue <- result, do: issue["created_at"]
    assert issues == ~w(a b c)
  end

  defp fake_created_at_list(created_ats) do
    data = for created_at <- created_ats do
      [{"created_at", created_at}, {"other_data", "xxx"}]
    end
    convert_to_list_of_maps data
  end
end
